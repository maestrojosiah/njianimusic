<?php

namespace App\Twig;

use App\Manager\CartManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\ProductRepository;
use App\Entity\Orderr;
use App\Repository\OrderrRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class GlobalVars extends AbstractExtension
{
    public function __construct(private readonly UserRepository $usr, private readonly ProductRepository $productRepo, private readonly CartManager $cartManager, private readonly Security $security)
    {
    }

    public function getFunctions(): ?array
    {
        return [new TwigFunction('global_vars', $this->getGlobalVars(...))];
    }

    public function getGlobalVars($select)
    {
        $products = $this->productRepo->findAll();
        $user = $this->getUserFromSecurityContext();

        $url = parse_url((string) $_SERVER['REQUEST_URI']);

        $glVars = [];
        $glVars['products'] = $products;
        $glVars['cart'] = $this->cartManager->getCurrentCart($user);
        
        // $glVars['thisUrl'] = $url['path'];

        return $glVars[$select];

    }

     /**
     * Gets the currently logged-in user from the security context.
     *
     * @return UserInterface|null
     */
    private function getUserFromSecurityContext(): ?UserInterface
    {
        $token = $this->getSecurityToken();

        if ($token && $token->getUser() instanceof UserInterface) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * Gets the security token from the security context.
     *
     * @return TokenInterface|null
     */
    private function getSecurityToken(): ?TokenInterface
    {
        return $this->security->getToken();
    }


}
