<?php

namespace App\Entity;

use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'addresses')]
    private ?User $user = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $street_address = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $county = null;

    #[ORM\OneToMany(mappedBy: 'shipping_address', targetEntity: OrderHistory::class)]
    private Collection $orderHistories;

    #[ORM\OneToOne(mappedBy: 'default_shipping_address', cascade: ['persist', 'remove'])]
    private ?Preference $preference = null;

    #[ORM\OneToMany(mappedBy: 'shippingAddress', targetEntity: Orderr::class)]
    private Collection $orderrs;

    public function __construct()
    {
        $this->orderHistories = new ArrayCollection();
        $this->orderrs = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getStreetAddress();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getStreetAddress(): ?string
    {
        return $this->street_address;
    }

    public function setStreetAddress(?string $street_address): static
    {
        $this->street_address = $street_address;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): static
    {
        $this->county = $county;

        return $this;
    }

    /**
     * @return Collection<int, OrderHistory>
     */
    public function getOrderHistories(): Collection
    {
        return $this->orderHistories;
    }

    public function addOrderHistory(OrderHistory $orderHistory): static
    {
        if (!$this->orderHistories->contains($orderHistory)) {
            $this->orderHistories->add($orderHistory);
            $orderHistory->setShippingAddress($this);
        }

        return $this;
    }

    public function removeOrderHistory(OrderHistory $orderHistory): static
    {
        if ($this->orderHistories->removeElement($orderHistory)) {
            // set the owning side to null (unless already changed)
            if ($orderHistory->getShippingAddress() === $this) {
                $orderHistory->setShippingAddress(null);
            }
        }

        return $this;
    }

    public function getPreference(): ?Preference
    {
        return $this->preference;
    }

    public function setPreference(?Preference $preference): static
    {
        // unset the owning side of the relation if necessary
        if ($preference === null && $this->preference !== null) {
            $this->preference->setDefaultShippingAddress(null);
        }

        // set the owning side of the relation if necessary
        if ($preference !== null && $preference->getDefaultShippingAddress() !== $this) {
            $preference->setDefaultShippingAddress($this);
        }

        $this->preference = $preference;

        return $this;
    }

    /**
     * @return Collection<int, Orderr>
     */
    public function getOrderrs(): Collection
    {
        return $this->orderrs;
    }

    public function addOrderr(Orderr $orderr): static
    {
        if (!$this->orderrs->contains($orderr)) {
            $this->orderrs->add($orderr);
            $orderr->setShippingAddress($this);
        }

        return $this;
    }

    public function removeOrderr(Orderr $orderr): static
    {
        if ($this->orderrs->removeElement($orderr)) {
            // set the owning side to null (unless already changed)
            if ($orderr->getShippingAddress() === $this) {
                $orderr->setShippingAddress(null);
            }
        }

        return $this;
    }
}
