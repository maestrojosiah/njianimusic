<?php

namespace App\Entity;

use App\Repository\OrderItemRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OrderItemRepository::class)]
class OrderItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'orderItems')]
    private ?Orderr $orderr = null;

    #[ORM\ManyToOne(inversedBy: 'orderItems')]
    private ?Product $product = null;


    #[ORM\Column(nullable: true)]
    #[Assert\NotBlank]
    #[Assert\GreaterThanOrEqual(1)]
    private ?int $quantity = null;

    #[ORM\Column(nullable: true)]
    private ?float $price_per_unit = null;

    #[ORM\Column(nullable: true)]
    private ?float $total_price = null;

    #[ORM\Column(nullable: true)]
    private ?float $discount_amount = null;

    #[ORM\Column(nullable: true)]
    private ?float $tax_amount = null;

    #[ORM\Column(nullable: true)]
    private ?float $subtotal = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $product_variant = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_gift = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $gift_message = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $notes = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $shipping_status = null;

    #[ORM\ManyToOne(inversedBy: 'orderItems')]
    private ?User $user = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $delivery_date = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $payment_date = null;

    /**
     * Tests if the given item given corresponds to the same order item.
     *
     * @param OrderItem $item
     *
     * @return bool
     */
    public function equals(OrderItem $item): bool
    {
        return $this->getProduct()->getId() === $item->getProduct()->getId();
    }
    
    public function getTotal(): ?float
    {
        if ($this->quantity === null || $this->price_per_unit === null) {
            return null;
        }
    
        $subtotal = $this->quantity * $this->price_per_unit;
    
        // Apply discount
        if ($this->discount_amount !== null) {
            $subtotal -= $this->discount_amount;
        }
    
        // Apply tax
        if ($this->tax_amount !== null) {
            $subtotal += $this->tax_amount;
        }
    
        // Update the total_price property
        $this->total_price = $subtotal;
    
        return $subtotal;
    }
    
    public function calculateTotalPrice(): ?float
    {
        if ($this->quantity === null || $this->price_per_unit === null) {
            return null;
        }

        $subtotal = $this->quantity * $this->price_per_unit;

        // Include tax if tax amount is set
        if ($this->tax_amount !== null) {
            $subtotal += $this->tax_amount;
        }

        return $subtotal;
    }

    public function calculateSubtotal(): ?float
    {
        if ($this->total_price === null || $this->discount_amount === null) {
            return null;
        }

        $subtotal = $this->total_price - $this->discount_amount;

        // Exclude tax if tax amount is set
        if ($this->tax_amount !== null) {
            $subtotal -= $this->tax_amount;
        }

        return $subtotal;
    }

    public function __toString(): string
    {
        return $this->getOrderr()->getTrackingNumber();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getOrderr(): ?Orderr
    {
        return $this->orderr;
    }

    public function setOrderr(?Orderr $orderr): static
    {
        $this->orderr = $orderr;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): static
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): static
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPricePerUnit(): ?float
    {
        return $this->price_per_unit;
    }

    public function setPricePerUnit(?float $price_per_unit): static
    {
        $this->price_per_unit = $price_per_unit;

        return $this;
    }

    public function getTotalPrice(): ?float
    {
        return $this->total_price;
    }

    public function setTotalPrice(?float $total_price): static
    {
        $this->total_price = $total_price;

        return $this;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discount_amount;
    }

    public function setDiscountAmount(?float $discount_amount): static
    {
        $this->discount_amount = $discount_amount;

        return $this;
    }

    public function getTaxAmount(): ?float
    {
        return $this->tax_amount;
    }

    public function setTaxAmount(?float $tax_amount): static
    {
        $this->tax_amount = $tax_amount;

        return $this;
    }

    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    public function setSubtotal(?float $subtotal): static
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getProductVariant(): ?string
    {
        return $this->product_variant;
    }

    public function setProductVariant(?string $product_variant): static
    {
        $this->product_variant = $product_variant;

        return $this;
    }

    public function isIsGift(): ?bool
    {
        return $this->is_gift;
    }

    public function setIsGift(?bool $is_gift): static
    {
        $this->is_gift = $is_gift;

        return $this;
    }

    public function getGiftMessage(): ?string
    {
        return $this->gift_message;
    }

    public function setGiftMessage(?string $gift_message): static
    {
        $this->gift_message = $gift_message;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): static
    {
        $this->notes = $notes;

        return $this;
    }

    public function getShippingStatus(): ?string
    {
        return $this->shipping_status;
    }

    public function setShippingStatus(?string $shipping_status): static
    {
        $this->shipping_status = $shipping_status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->delivery_date;
    }

    public function setDeliveryDate(?\DateTimeInterface $delivery_date): static
    {
        $this->delivery_date = $delivery_date;

        return $this;
    }

    public function getPaymentDate(): ?\DateTimeInterface
    {
        return $this->payment_date;
    }

    public function setPaymentDate(?\DateTimeInterface $payment_date): static
    {
        $this->payment_date = $payment_date;

        return $this;
    }    
}
