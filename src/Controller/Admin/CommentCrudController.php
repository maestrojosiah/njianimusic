<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Comment::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Comment();
        $template->setUser($this->getUser());
        $template->setCommentDate(new \DateTime());

        return $template;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Comment Details');
        yield AssociationField::new("blog")->setLabel("Blog");
        yield AssociationField::new("user")->setLabel("Author")
            ->autocomplete()
            ->onlyOnDetail();
        yield TextareaField::new("content")->setLabel("Content");
        yield DateField::new("commentDate")->setLabel("Commented on")
            ->onlyOnDetail();

    }

}
