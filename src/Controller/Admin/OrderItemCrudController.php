<?php

namespace App\Controller\Admin;

use App\Entity\OrderItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class OrderItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OrderItem::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("user")->setLabel("User")
            ->autocomplete();
        yield AssociationField::new("orderr")->setLabel("Order");
        yield AssociationField::new("product")->setLabel("Product");
        yield IntegerField::new("quantity")->setLabel("Quantity");
        yield IntegerField::new("pricePerUnit")->setLabel("Price @");
        yield IntegerField::new("totalPrice")->setLabel("Total Price");
        yield IntegerField::new("discountAmount")->setLabel("Discount")
            ->hideOnIndex();
        yield IntegerField::new("taxAmount")->setLabel("Tax")
            ->hideOnIndex();
        yield IntegerField::new("subtotal")->setLabel("Subtotal")
            ->hideOnIndex();
        yield TextField::new("productVariant")->setLabel("Product Variant")
            ->hideOnIndex();
        yield BooleanField::new("isGift")->setLabel("Is Gift?")
            ->hideOnIndex();
        yield TextareaField::new("notes")->setLabel("Notes")
            ->hideOnIndex();
        yield TextareaField::new("giftMessage")->setLabel("Gift Message")
            ->hideOnIndex();
        yield TextField::new("shippingStatus")->setLabel("Shipping Status")
            ->hideOnIndex();
        yield DateField::new("deliveryDate")->setLabel("Delivery Date")
            ->hideOnIndex();
        yield DateField::new("paymentDate")->setLabel("Payment Date")
            ->hideOnIndex();
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Order Item")
            ->setEntityLabelInPlural("Order Items");

    }

    public function configureActions(Actions $actions): Actions
    {
        return parent::configureActions($actions)
            ->remove(Crud::PAGE_INDEX, Action::NEW);
    }

}
