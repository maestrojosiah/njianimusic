<?php

namespace App\Controller\Admin;

use App\Entity\FeaturedBlog;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FeaturedBlogCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return FeaturedBlog::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel('Featured Blog Info');
        yield AssociationField::new("blog")->setLabel("Blog");
        yield DateField::new("startDate")->setLabel("Starting From");
        yield DateField::new("endDate")->setLabel("Ending On");
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Featured Blog")
            ->setEntityLabelInPlural("Featured Blogs");

    }

}
