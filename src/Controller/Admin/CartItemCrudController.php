<?php

namespace App\Controller\Admin;

use App\Entity\CartItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CartItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CartItem::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield AssociationField::new("cart")->setLabel("Cart")
            ->autocomplete();
        yield AssociationField::new("product")->setLabel("Product");
        yield IntegerField::new("quantity")->setLabel("Quantity");
        yield IntegerField::new("pricePerItem")->setLabel("Price Per Item");
        yield IntegerField::new("subtotal")->setLabel("Subtotal");
    }

}
