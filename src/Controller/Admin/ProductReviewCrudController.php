<?php

namespace App\Controller\Admin;

use App\Entity\ProductReview;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductReviewCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ProductReview::class;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6);
        yield TextField::new("reviewTitle")->setLabel("Review Title");
        yield AssociationField::new("product")->setLabel("Product")
            ->autocomplete();
        yield AssociationField::new("user")->setLabel("User")
            ->autocomplete();
        $ratings = ['1','2','3','4','5'];
        yield BooleanField::new("isVerifiedPurchase")->setLabel("Is Verified?");
        yield ChoiceField::new("rating")
            ->setLabel("Rating")
            ->setChoices(array_combine($ratings, $ratings))
            ->setEmptyData(5)
            ->renderExpanded(true);
        yield DateField::new("reviewDate")->setLabel("Review Date");

        yield FormField::addColumn(6);
        yield TextareaField::new("reviewText")->setLabel("Review Text")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
        ;
        yield TextareaField::new("response")->setLabel("Response")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
        ;
        yield ChoiceField::new("reviewStatus")
            ->setLabel("Review Status")
            ->setChoices([
                "Public"=> "public",
                "Hidden"=> "hidden",
            ])
            ->addCssClass("mb-5");
        

    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Product Review")
            ->setEntityLabelInPlural("Product Reviews")
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);

    }

}
