<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Category::class;
    }


    public function configureFields(string $pageName): iterable
    {

        yield FormField::addColumn(6);
        yield TextField::new("categoryName")->setLabel("Category Name");
        yield TextField::new("slug")->setLabel("Slug");
        yield TextAreaField::new("description")->setLabel("Description")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
        yield ImageField::new("imageUrl")->setLabel("Image")
            ->setUploadDir("/public/site/images/uploads")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/uploads");
        yield BooleanField::new("isActive")->setLabel("Is Active");
        yield IntegerField::new("displayOrder")->setLabel("DisplayOrder");

    }

    
    public function configureActions(Actions $actions) : Actions
    {
        return parent::configureActions($actions)
        ->disable(Action::DETAIL);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setEntityLabelInSingular("Product Category")
            ->setEntityLabelInPlural("Product Categories")
;
    }


}
