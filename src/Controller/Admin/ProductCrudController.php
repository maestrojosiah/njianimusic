<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $template = new Product();
        $template->setDateAdded(new \DateTime());

        return $template;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addTab("Product Details");
        yield FormField::addColumn(6);
        yield TextField::new("productName")->setLabel("Name");
        yield TextField::new("brand")->setLabel("Brand")
            ->hideOnIndex();
        yield TextField::new("slug")->setLabel("Slug")
            ->hideOnIndex();
        yield AssociationField::new("categories")->setLabel("Categories")
            ->hideOnIndex();
        yield TextAreaField::new("description")->setLabel("Description")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
        yield ImageField::new("images")->setLabel("Image")
            ->setUploadDir("/public/site/images/products")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/products")
            ->hideOnIndex();

        yield FormField::addColumn(6);
        yield TextField::new("videoLink")->setLabel("Video Link")
            ->hideOnIndex();
        yield IntegerField::new("price")->setLabel("Price");
        yield AssociationField::new("productImages")->setLabel("Images");
        // yield IntegerField::new("stockQuantity")->setLabel("In Stock");
        yield TextField::new("images")->setLabel("Images")
            ->onlyOnDetail();
        yield TextField::new("rating")->setLabel("Rating")
            ->hideOnIndex();
        yield IntegerField::new("numberOfRatings")->setLabel("No. Of Ratings")
            ->hideOnIndex();
        yield BooleanField::new("availability")->setLabel("Is Available?")
            ->renderAsSwitch(false);
        yield TextareaField::new("features")->setLabel("Product Features")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
        yield IntegerField::new("weight")->setLabel("Weight")
            ->hideOnIndex();
        yield DateField::new("dateAdded")->setLabel("Added On")
            ->onlyOnDetail();
            
        yield FormField::addTab("Extra Details");
        yield BooleanField::new("isFeatured")->setLabel("Is Featured?")
            ->renderAsSwitch(false)
            ->hideOnIndex();
        yield BooleanField::new("isNew")->setLabel("Is New?")
            ->renderAsSwitch(false)
            ->hideOnIndex();
        yield BooleanField::new("isBestseller")->setLabel("Is Bestseller?")
            ->renderAsSwitch(false)
            ->hideOnIndex();
        yield BooleanField::new("isOnsale")->setLabel("Is On Sale?")
            ->renderAsSwitch(false)
            ->hideOnIndex();


    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }
    

}
