<?php

namespace App\Controller\Admin;

use App\Entity\WebInfo;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class WebInfoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return WebInfo::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6);
        yield TextField::new("type")->setLabel("Information Type");
        yield TextareaField::new("content")->setLabel("Content")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ])
        ;
    }

}
