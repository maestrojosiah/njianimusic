<?php

namespace App\Controller\Admin;

use App\Entity\Address;
use App\Entity\Blog;
use App\Entity\BlogCategory;
use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\CommentReplies;
use App\Entity\DiscountCode;
use App\Entity\DiscountUsage;
use App\Entity\FeaturedBlog;
use App\Entity\Media;
use App\Entity\Message;
use App\Entity\OrderHistory;
use App\Entity\OrderItem;
use App\Entity\Orderr;
use App\Entity\Preference;
use App\Entity\Product;
use App\Entity\ProductDisplay;
use App\Entity\ProductImage;
use App\Entity\ProductReview;
use App\Entity\Tag;
use App\Entity\User;
use App\Entity\WebInfo;
use App\Entity\Wishlist;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class DashboardController extends AbstractDashboardController
{

    public function __construct(private AdminUrlGenerator $adminUrlGenerator, private EntityManagerInterface $entityManager){}

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        // $chart = $this->chartBuilder->createChart(Chart::TYPE_LINE);

        // $chart->setData([
        //     'labels' => ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        //     'datasets' => [
        //         [
        //             'label' => 'My First dataset',
        //             'backgroundColor' => 'rgb(255, 99, 132)',
        //             'borderColor' => 'rgb(255, 99, 132)',
        //             'data' => [0, 10, 5, 2, 20, 30, 45],
        //         ],
        //     ],
        // ]);

        // $chart->setOptions([
        //     'scales' => [
        //         'y' => [
        //             'suggestedMin' => 0,
        //             'suggestedMax' => 100,
        //         ],
        //     ],
        // ]);


        // return parent::index();
        // return $this->render('admin/index.html.twig', [
        //     'chart' => $chart,
        // ]);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        return $this->redirect($this->adminUrlGenerator->setController(OrderrCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Njiani Music Shop')
            ->setFaviconPath('/site/favicon.ico')
            ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-dashboard');

        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            yield Menuitem::section('Orders');
            yield MenuItem::linkToCrud('Orders', 'fas fa-shopping-bag', Orderr::class);
            yield MenuItem::linkToCrud('Order Item', 'fas fa-shopping-cart', OrderItem::class);
            yield MenuItem::linkToCrud('Address', 'fas fa-map-marker', Address::class);
        } else {
            yield Menuitem::section('Orders');
            yield MenuItem::linkToCrud('My Orders', 'fas fa-shopping-bag', Orderr::class);
        }

        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            yield Menuitem::section('Blog');
            yield MenuItem::linkToCrud('Blog', 'fas fa-pen', Blog::class);
            yield MenuItem::linkToCrud('Blog Categories', 'fas fa-folder', BlogCategory::class);
            yield MenuItem::linkToCrud('Featured Blog', 'fas fa-pen', FeaturedBlog::class);
            yield MenuItem::linkToCrud('Comment', 'fas fa-comment', Comment::class);
            yield MenuItem::linkToCrud('Comment Replies', 'fas fa-comments', CommentReplies::class);
            // yield MenuItem::linkToCrud('Featured Post', 'fas fa-pen', FeaturedPost::class);
            yield MenuItem::linkToCrud('Tag', 'fas fa-tag', Tag::class);
        } else {
            // nothing
        }

        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            yield Menuitem::section('Products');
            yield MenuItem::linkToCrud('Product', 'fas fa-box', Product::class);
            yield MenuItem::linkToCrud('Product Categories', 'fas fa-folder', Category::class);
            yield MenuItem::linkToCrud('Product Display', 'fas fa-desktop', ProductDisplay::class);
            yield MenuItem::linkToCrud('Product Image', 'fas fa-image', ProductImage::class);
            yield MenuItem::linkToCrud('Product Review', 'fas fa-star', ProductReview::class);
            yield MenuItem::linkToCrud('Wishlist', 'fas fa-heart', Wishlist::class);
            // yield MenuItem::linkToCrud('Media', 'fas fa-film', Media::class);
            // yield MenuItem::linkToCrud('Order History', 'fas fa-clock', OrderHistory::class);
        } else {
            // nothing
        }

        if ($this->isGranted('ROLE_CONTRIBUTOR') ) {
            
            yield Menuitem::section('More');
            yield MenuItem::linkToCrud('Messages', 'fa fa-envelope-o', Message::class);
            yield MenuItem::linkToCrud('Web Information', 'fa fa-info', WebInfo::class);
            // yield MenuItem::linkToCrud('Preference', 'fas fa-gear', Preference::class);
            yield MenuItem::linkToCrud('User', 'fas fa-users', User::class);
            yield MenuItem::linkToUrl('Homepage', 'fa fa-home', $this->generateUrl('app_default'));
            // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
        } else {
            yield Menuitem::section('More');
            yield MenuItem::linkToCrud('Me', 'fas fa-users', User::class);
            yield MenuItem::linkToUrl('Homepage', 'fa fa-home', $this->generateUrl('app_default'));
        }
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setCssClass('btn btn-sm btn-info')->setHtmlAttributes(['title' => 'View'])->setIcon('fa fa-eye')->setLabel("View");
            }) 
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setCssClass('btn btn-sm btn-primary')->setHtmlAttributes(['title' => 'Edit'])->setIcon('fa fa-edit')->setLabel("Edit");
            }) 
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setCssClass('btn btn-sm btn-outline-secondary action-delete')->setHtmlAttributes(['title' => 'Delete'])->setIcon('fa fa-trash')->setLabel("Delete");
            }) ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        if (!$user instanceof User) {
            throw new \Exception('Wrong user');
        }

        return parent::configureUserMenu($user)
            ->setAvatarUrl("/site/images/profile_pictures/".$user->getAvatarUri())
            ->setMenuItems([
                MenuItem::linkToUrl('My Profile','fas fa-user', $this->generateUrl('app_profile_show')),
                MenuItem::linkToUrl('Logout', 'fa fa-sign-out', $this->generateUrl('app_logout')),
            ]);

    }

    // public function configureAssets(): Assets
    // {
    //     return parent::configureAssets()
    //         ->addCssFile('/site/css/test.css'); 
    // }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setPageTitle('index', '%entity_label_plural% listing')
            ->showEntityActionsInlined();
    }    

    public function configureAssets(): Assets
    {
        // return parent::configureAssets()
            // ->addCssFile('styles/app.css')
            // ->addJsFile('app.js')
            // ->addJsFile('vendor/chart.js/auto.js');
            // ->addJsFile('/invoices/js/jquery-1.9.1.js')
            // ->addJsFile('/invoices/js/spectrum.js')
            // ->addJsFile('/invoices/js/configSpectrum.js');
        $assets = parent::configureAssets()
            ->addCssFile('site/css/material-dashboard.css')
            ->addCssFile('site/css/bootstrap.min.css');
        // $assets->addAssetMapperEntry('app');
        return $assets;
    }


}
