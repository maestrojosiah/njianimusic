<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Message;
use App\Form\CartType;
use App\Manager\CartManager;
use App\Repository\AddressRepository;
use App\Repository\DeliveriesRepository;
use App\Repository\OrderrRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{

    public function __construct(private readonly SeoGeneratorProvider $seoGenerator, private readonly DeliveriesRepository $deliveriesRepository, private readonly EntityManagerInterface $em, private readonly UserRepository $userRepo, private readonly Mailer $mailer)
    {}

    #[Route('/cart', name: 'cart')]
    public function index(CartManager $cartManager, Request $request): Response
    {
        $cart = $cartManager->getCurrentCart($this->getUser());
        
        $form = $this->createForm(CartType::class, $cart);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $cart = $form->getData();
            // echo "<pre> from cart index";
            // var_dump($cart->getId());
            // print_r(count($form->getData()->getOrderItems()) );
            foreach ($form->getData()->getOrderItems() as $item) {
                // echo $item->getQuantity()."<br>";
                $totalPrice = $this->calculateTotalPrice($item->getQuantity(), $item->getPricePerUnit());
                $subtotal = $this->calculateSubtotal($totalPrice, 0);
                $item->setTotalPrice($totalPrice);
                $item->setSubtotal($subtotal);
                // print_r($totalPrice);
                // print_r($subtotal);
                // echo "<br>";
            }
            // die();
            // die();
            $cart->setOrderDate(new \DateTime());
            $cartManager->save($cart);

            return $this->redirectToRoute('cart');
        }

        $this->seoGenerator->get('basic')
            ->setTitle('Your Cart')
            ->setDescription('Complete your order')
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');
       
        if ($cart->getOrderItems()->count() < 1) {
            return $this->redirectToRoute('app_default');
        }
    
        return $this->render('cart/index.html.twig', [
            'cart' => $cart,
            'form' => $form->createView()
        ]);
    }


    #[Route('/checkout', name:'checkout')]
    public function checkout(CartManager $cartManager, Request $request): Response
    {
        if (!$this->isGranted('ROLE_USER')) {
            
            $this->addFlash(
                'success',
                'Login if you have an account with us. If not, please create an account to complete the purchase.'
            );
    

        }

        $this->denyAccessUnlessGranted('ROLE_USER');
        $cart = $cartManager->getCurrentCart($this->getUser());

        $deliveries = $this->deliveriesRepository->findAll();
        $optgroups = [];

        foreach ($deliveries as $delivery) {
            $optgroups[$delivery->getOptgroup()][$delivery->getId()] = $delivery->getArea();
        }


        $this->seoGenerator->get('basic')
            ->setTitle('Checkout')
            ->setDescription('Checkout')
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');
            
        return $this->render('cart/checkout.html.twig', [
            'cart'=> $cart,
            'user' => $this->getUser(),
            'deliveries' => $deliveries,
            'optgroups' => $optgroups
        ]);

    }

    #[Route('/order/received/successfully/{order_id}/{shipping}', name:'order_received')]
    public function orderReceived(CartManager $cartManager, OrderrRepository $orderRepo, Request $request, $order_id, $shipping): Response
    {

        $this->denyAccessUnlessGranted('ROLE_USER');
        $cart = $orderRepo->findOneById($order_id);


        $this->seoGenerator->get('basic')
            ->setTitle('Order Received')
            ->setDescription('Your order has been successfully received. We will give you a call shortly')
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');
            
        return $this->render('cart/order_received.html.twig', [
            'cart'=> $cart,
            'shipping' => $shipping,
            'user' => $this->getUser(),
        ]);

    }

    public function calculateTotalPrice($quantity, $price_per_unit, $tax_amount = null): ?float
    {
        if ($quantity === null || $price_per_unit === null) {
            return null;
        }

        $subtotal = $quantity * $price_per_unit;

        // Include tax if tax amount is set
        if ($tax_amount !== null) {
            $subtotal += $tax_amount;
        }

        return $subtotal;
    }

    public function calculateSubtotal($total_price, $discount_amount, $tax_amount = null): ?float
    {
        if ($total_price === null || $discount_amount === null) {
            return null;
        }

        $subtotal = $total_price - $discount_amount;

        // Exclude tax if tax amount is set
        if ($tax_amount !== null) {
            $subtotal -= $tax_amount;
        }

        return $subtotal;
    }

    #[Route('/submit/checkout', name:'submit_checkout')]
    public function submitCheckout(CartManager $cartManager, Request $request, AddressRepository $addressRepo): Response
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
            // Check which submit button was clicked
            if (isset($_POST['payOnDelivery'])) {

                $this->denyAccessUnlessGranted('ROLE_USER');
                $date = new \DateTime();
                $firstName = $_POST['firstName'];
                $lastName = $_POST['lastName'];
                $streetAddress = $_POST['streetAddress'];
                $shipping_cost = $_POST['shipping'];
                $cartAndShipping = $_POST['cartAndShipping'];
                $county = $_POST['county'];
                $phoneNumber = $_POST['phoneNumber'];
                $fullname = $firstName . ' ' . $lastName;
                $user = $this->getUser();

                $existingAddress = $addressRepo->findOneByUser($user);
                
                if($existingAddress === null) {
                    $address = new Address();
                } else {
                    $address = $existingAddress;
                }
                
                $address->setUser($user);
                $address->setStreetAddress($streetAddress);
                $address->setCounty($county);
                $this->em->persist($address);
                $this->em->flush();

                $msg = "There is a new order: Name: $fullname, Street Address: $streetAddress, County: $county, Phone Number: $phoneNumber ";

                $cart = $cartManager->getCurrentCart($this->getUser());
                
                $cart->setOrderStatus('processing');
                // echo "<pre>";
                // var_dump($cart);
                // die();
                $cart->setTotalAmount($cartAndShipping);
                $cart->setTaxAmount(0);
                $cart->setShippingAmount($shipping_cost);
                $cart->setSubtotal($cart->getTotal());
                $cart->setDiscountAmount(0);
                $cart->setShippingMethod('delivery');
                $cart->setPaymentStatus('pending');
                // $cart->setPaymentDate();
                // $cart->setNotes();
                // $cart->setPromoCode();
                $cart->setDeliveryStatus('in_transit');
                $cart->setShippingAddress($address);
                
                $message = new Message();
                $message->setName($fullname);
                $message->setEmail($user->getEmail());
                $message->setMessage($msg);
                $message->setReceivedOn($date);
                $this->em->persist($message);
                $this->em->flush();
    
                $subject = "New Order From Website";
                $admins = $this->userRepo->findByUsertype('admin');
                $maildata = ['name' => $fullname, 'emailAd' => $user->getEmail(), 'subject' => $subject, 'cartAndShipping' => $cartAndShipping, 'message' => $msg];
                
                foreach ($admins as $admin) {
                    $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "orderform.html.twig");
                }
                $this->mailer->sendEmailMessage($maildata, $user->getEmail(), "Your order has been received", "order_received.html.twig");
        
                $this->addFlash('success','Your Order has been received. We will call you soon');
        

            
                return $this->redirectToRoute('order_received', ['order_id' => $cart->getId(), 'shipping' => $shipping_cost]);
                        
                // pay on delivery was clicked
                // save all required data
                // send email to admins and client
                // got to confirmation page and show necessary info
            }
        }
    
        return $this->render('payment_page.html.twig', [
            'test'=> $request->get('test'),
        ]);
    }

}
