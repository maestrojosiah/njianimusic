<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function __construct(private readonly SeoGeneratorProvider $seoGenerator){}

    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser() && $this->getUser()->getUserType() == 'user') {
            return $this->redirectToRoute('cart');
        } elseif ($this->getUser() && $this->getUser()->getUserType() == 'admin') {
            return $this->redirectToRoute('admin');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $this->seoGenerator->get('basic')
            ->setTitle('Login to help us remember you.')
            ->setDescription('Explore a wide range of music instruments online. Order and pay on delivery. Expert advice on the best instruments for your needs.')
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
