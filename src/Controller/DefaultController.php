<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\ContactType;
use App\Repository\CategoryRepository;
use App\Repository\ProductDisplayRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use App\Repository\WebInfoRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Leogout\Bundle\SeoBundle\Provider\SeoGeneratorProvider;

class DefaultController extends AbstractController
{

    public function __construct(private readonly SeoGeneratorProvider $seoGenerator, private readonly EntityManagerInterface $em, private readonly Mailer $mailer, private readonly WebInfoRepository $webInfoRepo){}

    #[Route('/', name: 'app_default')]
    public function index(ProductRepository $productRepo, CategoryRepository $categoryRepo, ProductDisplayRepository $productDisplayRepo, Request $request): Response
    {

        $products = $productRepo->findBy(
            ['availability' => true]
        );
        $categories = $categoryRepo->findBy(
            ['is_active' => true]
        );
        $productDisplays = $productDisplayRepo->findBy(
            ['label' => 'thin'],
            ['id'=> 'ASC']
        );
        $this->seoGenerator->get('basic')
            ->setTitle('Musical Instruments Shop Nairobi | Online Music Shop')
            ->setDescription("Nairobi's ultimate online music store! Shop guitars, keyboards, drums, violins, pianos, trumpets & instruments for every genre. Fast delivery, widest selection in Nairobi. Get expert advise on the best instrument for your needs.")
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');
                        
        return $this->render('default/index.html.twig', [
            'products' => $products,
            'productdisplays' => $productDisplays,
            'categories' => $categories,
        ]);
    }

    #[Route('/show/profile', name: 'app_profile_show')]
    public function profile(): Response
    {
        return $this->render('default/profile.html.twig', [
            'test' => 'Test',
        ]);

    }

    #[Route('/about', name: 'app_about')]
    public function about(): Response
    {
        $about = $this->webInfoRepo->findOneByType('about');

        $this->seoGenerator->get('basic')
            ->setTitle('About Njiani Music Shop')
            ->setDescription("Nairobi's ultimate online music store! Shop guitars, keyboards, drums, violins, pianos, trumpets & instruments for every genre. Fast delivery, widest selection in Nairobi. Shop now & rock your Nairobi rhythm!")
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');

        return $this->render('default/about.html.twig', [
            'about' => $about,
        ]);

    }

    #[Route('/faq', name: 'app_faq')]
    public function faq(): Response
    {
        $faq = $this->webInfoRepo->findOneByType('faq');

        $this->seoGenerator->get('basic')
            ->setTitle('Frequently Asked Questions (FAQs)')
            ->setDescription("Nairobi's ultimate online music store! Shop guitars, keyboards, drums, violins, pianos, trumpets & instruments for every genre. Fast delivery, widest selection in Nairobi. Shop now & rock your Nairobi rhythm!")
            ->setKeywords('music instruments, online instrument store, instrument delivery, pay on delivery, musical gear, expert advice, melody shop, rhythm delivered, harmonic choices, instrument recommendations, quality sound, express music, reliable delivery, musical instruments online, easy ordering, tune advisory, music gear hub, melodic express, order and play, instrument consultation');

        return $this->render('default/faq.html.twig', [
            'faq' => $faq,
        ]);

    }

    public function renderFooterForm(): Response
    {
        $form = $this->createForm(ContactType::class);
        
        return $this->render('tmp/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    #[Route('/send/message/contact/form', name: 'contact_form')]
    public function contact_form(UserRepository $userRepo, Request $request): Response
    {

        // Create an instance of the form
        $form = $this->createForm(ContactType::class);
        
        // Handle form submission
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $name = $contact['name'];
            $email = $contact['email'];
            $msg = $contact['message'];
            $date = new \DateTime();

            
            $message = new Message();
            $message->setName($name);
            $message->setEmail($email);
            $message->setMessage($msg);
            $message->setReceivedOn($date);
            $this->em->persist($message);
            $this->em->flush();

            $subject = "Message From Contact Form";
            $admins = $userRepo->findByUsertype('admin');
            $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => $msg];

            foreach ($admins as $admin) {
                $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
            }
            $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");
    
            $this->addFlash('success','Your Message has been sent');

        } else {

            $this->addFlash('danger','Your Message has NOT been sent');

        }
   
        return $this->redirectToRoute('app_default');

    }

    #[Route(path: '/test/test/test', name: 'multitesting')]
    public function testingmultiplethings(): Response
    {

        $arr = ["config/packages/security.yaml", "public/site/css/bootstrap.css", "public/site/css/style.css", "public/site/single-product.html", "src/Controller/Admin/AddressCrudController.php", "src/Controller/Admin/BlogCategoryCrudController.php", "src/Controller/Admin/BlogCrudController.php", "src/Controller/Admin/CategoryCrudController.php", "src/Controller/Admin/CommentCrudController.php", "src/Controller/Admin/CommentRepliesCrudController.php", "src/Controller/Admin/DashboardController.php", "src/Controller/Admin/FeaturedBlogCrudController.php", "src/Controller/Admin/MessageCrudController.php", "src/Controller/Admin/OrderHistoryCrudController.php", "src/Controller/Admin/OrderItemCrudController.php", "src/Controller/Admin/OrderrCrudController.php", "src/Controller/Admin/ProductCrudController.php", "src/Controller/Admin/ProductDisplayCrudController.php", "src/Controller/Admin/ProductImageCrudController.php", "src/Controller/Admin/ProductReviewCrudController.php", "src/Controller/Admin/UserCrudController.php", "src/Controller/Admin/WebInfoCrudController.php", "src/Controller/Admin/WishlistCrudController.php", "src/Controller/DefaultController.php", "src/Controller/RegistrationController.php", "src/Security/AppCustomAuthenticator.php", "src/Controller/Admin/DiscountCodeCrudController.php", "src/Controller/Admin/DiscountUsageCrudController.php", "templates/admin/ckeditor_edit.html.twig", "templates/admin/ckeditor_new.html.twig", "templates/admin/fields/discounted_products.html.twig", "templates/admin/fields/raw_content.html.twig", "templates/admin/fields/raw_desc.html.twig", "templates/admin/fields/shortened_text.html.twig"];

        $proj_dir = $this->getParameter('proj_dir');
        $test = [];
        foreach($arr as $a) {
            $src = $proj_dir . "/" . $a;
            $sections = explode("/", $a);
            $file = array_pop($sections);
            $ext = explode(".", $file, 2)[1];
            $destfolder = $proj_dir . "/test";
            foreach ($sections as $section) {
                $destfolder .= "/".$section;
            }
            if (!file_exists($destfolder)) {
                mkdir($destfolder, 0775, true);
            }
            $dest = $destfolder . "/" . $file;
            copy($src, $destfolder . "/" . $file);

            $test[] = $dest;
        }

        return $this->render('default/test2.html.twig', [
            'arr' => $arr,
            'statements' => $test,
        ]);
    }


}
