<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231109061709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, street_address VARCHAR(255) DEFAULT NULL, county VARCHAR(255) DEFAULT NULL, INDEX IDX_D4E6F81A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, category_name VARCHAR(255) DEFAULT NULL, description VARCHAR(600) NOT NULL, image_url VARCHAR(255) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, display_order VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_history (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, shipping_address_id INT DEFAULT NULL, order_status VARCHAR(255) DEFAULT NULL, payment_method VARCHAR(255) DEFAULT NULL, total_amount VARCHAR(255) DEFAULT NULL, shipping_method VARCHAR(255) DEFAULT NULL, payment_status VARCHAR(255) DEFAULT NULL, payment_date VARCHAR(255) DEFAULT NULL, date_of_payment DATETIME DEFAULT NULL, delivery_date DATETIME DEFAULT NULL, notes LONGTEXT DEFAULT NULL, promo_code VARCHAR(255) DEFAULT NULL, order_discount VARCHAR(255) DEFAULT NULL, tax_amount VARCHAR(255) DEFAULT NULL, subtotal VARCHAR(255) DEFAULT NULL, INDEX IDX_D1C0D900A76ED395 (user_id), INDEX IDX_D1C0D9004D4CFF2B (shipping_address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_item (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, orderr_id INT DEFAULT NULL, product_id INT DEFAULT NULL, quantity VARCHAR(255) DEFAULT NULL, price_per_unit VARCHAR(255) DEFAULT NULL, total_price VARCHAR(255) DEFAULT NULL, discount_amount VARCHAR(255) DEFAULT NULL, tax_amount VARCHAR(255) DEFAULT NULL, subtotal VARCHAR(255) DEFAULT NULL, product_variant VARCHAR(255) DEFAULT NULL, is_gift TINYINT(1) DEFAULT NULL, gift_message VARCHAR(255) DEFAULT NULL, notes LONGTEXT DEFAULT NULL, shipping_status VARCHAR(255) DEFAULT NULL, delivery_date VARCHAR(255) DEFAULT NULL, INDEX IDX_52EA1F09A76ED395 (user_id), INDEX IDX_52EA1F097742FDB3 (orderr_id), INDEX IDX_52EA1F094584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orderr (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, shipping_address_id INT DEFAULT NULL, order_status VARCHAR(255) DEFAULT NULL, payment_method VARCHAR(255) DEFAULT NULL, total_amount VARCHAR(255) DEFAULT NULL, tax_amount VARCHAR(255) DEFAULT NULL, shipping_amount VARCHAR(255) DEFAULT NULL, subtotal VARCHAR(255) DEFAULT NULL, discount_amount VARCHAR(255) DEFAULT NULL, tracking_number VARCHAR(255) DEFAULT NULL, shipping_method VARCHAR(255) DEFAULT NULL, payment_status VARCHAR(255) DEFAULT NULL, payment_date DATETIME DEFAULT NULL, notes LONGTEXT DEFAULT NULL, promo_code VARCHAR(255) DEFAULT NULL, delivery_status VARCHAR(255) DEFAULT NULL, delivery_date VARCHAR(255) DEFAULT NULL, INDEX IDX_9228CD78A76ED395 (user_id), INDEX IDX_9228CD784D4CFF2B (shipping_address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE preference (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, default_shipping_address_id INT DEFAULT NULL, language VARCHAR(255) DEFAULT NULL, notification_settings VARCHAR(255) DEFAULT NULL, communication_pref VARCHAR(255) DEFAULT NULL, privacy_settings VARCHAR(255) DEFAULT NULL, email_preference VARCHAR(255) DEFAULT NULL, marketing_consent VARCHAR(255) DEFAULT NULL, INDEX IDX_5D69B053A76ED395 (user_id), UNIQUE INDEX UNIQ_5D69B053E4901476 (default_shipping_address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, product_name VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, price VARCHAR(255) DEFAULT NULL, discount_price VARCHAR(255) DEFAULT NULL, stock_quantity VARCHAR(255) DEFAULT NULL, images VARCHAR(500) DEFAULT NULL, rating VARCHAR(255) DEFAULT NULL, number_of_ratings VARCHAR(255) DEFAULT NULL, availability TINYINT(1) DEFAULT NULL, features LONGTEXT DEFAULT NULL, weight VARCHAR(255) DEFAULT NULL, is_featured TINYINT(1) DEFAULT NULL, is_new TINYINT(1) DEFAULT NULL, is_bestseller TINYINT(1) DEFAULT NULL, is_onsale TINYINT(1) DEFAULT NULL, date_added DATETIME DEFAULT NULL, INDEX IDX_D34A04AD12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_review (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, product_id INT DEFAULT NULL, rating VARCHAR(255) DEFAULT NULL, review_title VARCHAR(255) DEFAULT NULL, review_text LONGTEXT DEFAULT NULL, review_date DATETIME DEFAULT NULL, is_verified_purchase TINYINT(1) DEFAULT NULL, response LONGTEXT DEFAULT NULL, review_status VARCHAR(255) DEFAULT NULL, INDEX IDX_1B3FC062A76ED395 (user_id), INDEX IDX_1B3FC0624584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wishlist (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, quantity VARCHAR(255) DEFAULT NULL, added_at DATETIME DEFAULT NULL, notes VARCHAR(255) DEFAULT NULL, INDEX IDX_9CE12A31A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE order_history ADD CONSTRAINT FK_D1C0D900A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE order_history ADD CONSTRAINT FK_D1C0D9004D4CFF2B FOREIGN KEY (shipping_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F09A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F097742FDB3 FOREIGN KEY (orderr_id) REFERENCES orderr (id)');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F094584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE orderr ADD CONSTRAINT FK_9228CD78A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE orderr ADD CONSTRAINT FK_9228CD784D4CFF2B FOREIGN KEY (shipping_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE preference ADD CONSTRAINT FK_5D69B053A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE preference ADD CONSTRAINT FK_5D69B053E4901476 FOREIGN KEY (default_shipping_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE product_review ADD CONSTRAINT FK_1B3FC062A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_review ADD CONSTRAINT FK_1B3FC0624584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE wishlist ADD CONSTRAINT FK_9CE12A31A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD first_name VARCHAR(255) DEFAULT NULL, ADD last_name VARCHAR(255) DEFAULT NULL, ADD phone_number VARCHAR(255) DEFAULT NULL, ADD gender VARCHAR(255) DEFAULT NULL, ADD profile_picture VARCHAR(255) DEFAULT NULL, ADD status VARCHAR(255) DEFAULT NULL, ADD registration_date DATETIME DEFAULT NULL, ADD last_login_date VARCHAR(255) DEFAULT NULL, ADD last_login_day DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81A76ED395');
        $this->addSql('ALTER TABLE order_history DROP FOREIGN KEY FK_D1C0D900A76ED395');
        $this->addSql('ALTER TABLE order_history DROP FOREIGN KEY FK_D1C0D9004D4CFF2B');
        $this->addSql('ALTER TABLE order_item DROP FOREIGN KEY FK_52EA1F09A76ED395');
        $this->addSql('ALTER TABLE order_item DROP FOREIGN KEY FK_52EA1F097742FDB3');
        $this->addSql('ALTER TABLE order_item DROP FOREIGN KEY FK_52EA1F094584665A');
        $this->addSql('ALTER TABLE orderr DROP FOREIGN KEY FK_9228CD78A76ED395');
        $this->addSql('ALTER TABLE orderr DROP FOREIGN KEY FK_9228CD784D4CFF2B');
        $this->addSql('ALTER TABLE preference DROP FOREIGN KEY FK_5D69B053A76ED395');
        $this->addSql('ALTER TABLE preference DROP FOREIGN KEY FK_5D69B053E4901476');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD12469DE2');
        $this->addSql('ALTER TABLE product_review DROP FOREIGN KEY FK_1B3FC062A76ED395');
        $this->addSql('ALTER TABLE product_review DROP FOREIGN KEY FK_1B3FC0624584665A');
        $this->addSql('ALTER TABLE wishlist DROP FOREIGN KEY FK_9CE12A31A76ED395');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE order_history');
        $this->addSql('DROP TABLE order_item');
        $this->addSql('DROP TABLE orderr');
        $this->addSql('DROP TABLE preference');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_review');
        $this->addSql('DROP TABLE wishlist');
        $this->addSql('ALTER TABLE user DROP first_name, DROP last_name, DROP phone_number, DROP gender, DROP profile_picture, DROP status, DROP registration_date, DROP last_login_date, DROP last_login_day');
    }
}
